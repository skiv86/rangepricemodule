<?php
/**
 * Created by IntelliJ IDEA.
 * User: artyom
 * Date: 03.04.2019
 * Time: 14:13
 */

class AdminRangePriceModuleController extends ModuleAdminController
{
{

    public function __construct()
    {
        $this->lang = (!isset($this->context->cookie) || !is_object($this->context->cookie)) ? intval(Configuration::get('PS_LANG_DEFAULT')) : intval($this->context->cookie->id_lang);
        parent::__construct();
    }

    public function display(){
        parent::display();
    }

    public function renderView() {
        $html = '<p>';
        $html .= $this->l('This is the translatable text inside the paragraph');
        $html .= '</p>';
        return $html;
    }

}
}