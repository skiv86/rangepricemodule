<?php
if (!defined('_PS_VERSION_'))
  exit;
 
class RangePriceModule extends Module
{
  public function __construct()
  {
    $this->name = 'rangepricemodule';
    $this->tab = 'other';
    $this->version = '1.0.0';
    $this->author = '   Artsiom';
    $this->need_instance = 0;
    $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_); 
    $this->bootstrap = true;
 
    parent::__construct();
 
    $this->displayName = $this->l('My range price module');
    $this->description = $this->l('Description  ');
 
    $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
 
    if (!Configuration::get('RANGEPRICEMODULE_NAME'))
      $this->warning = $this->l('No name provided');
  }
public function install()
{
  if (Shop::isFeatureActive())
    Shop::setContext(Shop::CONTEXT_ALL);
 
  if (!parent::install() ||
    !$this->registerHook('leftColumn') ||
    !$this->registerHook('header') ||
    !$this->registerHook('footer') ||
    !$this->registerHook('home') ||
    !Configuration::updateValue('RANGEPRICEMODULE_NAME', 'my friend')
  )
    return false;
 
  return true;
}
public function uninstall()
{
  if (!parent::uninstall() ||
    !Configuration::deleteByName('RANGEPRICEMODULE_NAME')
  )
    return false;
 
  return true;
}

    public function getContent()
    {
        $output = null;

        if (Tools::isSubmit('submit'.$this->name)) {
            $myModuleName = strval(Tools::getValue('RANGEPRICEMODULE_NAME0'));
            $myModuleName2 = strval(Tools::getValue('RANGEPRICEMODULE_NAME2'));

            if (
                !$myModuleName ||
                empty($myModuleName) ||
                !Validate::isGenericName($myModuleName)
            ) {
                $output .= $this->displayError($this->l('Invalid Configuration value'));
            } else {
                Configuration::updateValue('RANGEPRICEMODULE_NAME0', $myModuleName);
                Configuration::updateValue('RANGEPRICEMODULE_NAME2', $myModuleName2);

                $output .= $this->displayConfirmation($this->l('Settings updated'));
            }
        }

        return $output.$this->displayForm();
    }

    public function displayForm()
    {
        // Get default language
        $defaultLang = (int)Configuration::get('PS_LANG_DEFAULT');

        // Init Fields form array
        $fieldsForm[0]['form'] = [
            'legend' => [
                'title' => $this->l('Settings'),
            ],
            'input' => [
                [
                    'type' => 'text',
                    'label' => $this->l('Цена ОТ '),
                    'name' => 'RANGEPRICEMODULE_NAME0',
                    'size' => 20,
                    'required' => true
                ],
                [
                    'type' => 'text',
                    'label' => $this->l('Цена ДО  '),
                    'name' => 'RANGEPRICEMODULE_NAME2',
                    'size' => 20,
                    'required' => true
                ]
            ],

            'submit' => [
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right'
            ]
        ];

        $helper = new HelperForm();

        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        // Language
        $helper->default_form_language = $defaultLang;
        $helper->allow_employee_form_lang = $defaultLang;

        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = [
            'save' => [
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                    '&token='.Tools::getAdminTokenLite('AdminModules'),
            ],
            'back' => [
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            ]
        ];

        // Load current value
        $helper->fields_value['RANGEPRICEMODULE_NAME0'] = Configuration::get('RANGEPRICEMODULE_NAME0');
        $helper->fields_value['RANGEPRICEMODULE_NAME2'] = Configuration::get('RANGEPRICEMODULE_NAME2');

        return $helper->generateForm($fieldsForm);
    }



	public function hookFooter()
    {
        $q1 = Configuration::get('RANGEPRICEMODULE_NAME0');
        $q2 = Configuration::get('RANGEPRICEMODULE_NAME2');
        if ((!$q2 || empty($q2) || ($q2 == " ")) or
            (!$q1 || empty($q1) || ($q1 == " "))) {
            $c1 = "Настройте плагин ";
            $this->context->smarty->assign('mess2', $c1);
            return $this->display(__FILE__, 'rangepricemodule.tpl');
        } else {
            $query = 'SELECT price FROM ps_product_shop     WHERE price BETWEEN ' . $q1 . ' AND ' . $q2 . ' ';
            $res = Db::getInstance()->executeS($query);
            $c1 = count($res);
            $this->context->smarty->assign('mess2', $c1);
            return $this->display(__FILE__, 'rangepricemodule.tpl');
        }
    }

}